var $route = (function(){

    var self = this;
    var _routes = [];

    var keys = [];

    var pathToRegExp = function(path){
        var routeMatcher = new RegExp('^'+path
            .replace(/\//g,'\\/')
            .replace(/:[^\s/]+/g, function(_, slash, key){

                keys.push({ name: _});
                return '([\\w-]+)';

            })+'$','i');

        return routeMatcher;
    }

    var getParams = function(path){
        var params = [];
            path.replace(/:[^\s/]+/g, function(key, slash){

                var param = key.replace(':','').replace('/','');
                params.push(param);

            });
        return params;
    }

    var urlChange = function(){
        var route = window.location.hash.substring(1);
        var params = [];

        var selectedRoute = _routes.filter(function (value) {

            var matchRouter = route.match(value.regex);

            if (route.match(value.regex)){

                if (value.params.length > 0){
                    var i = 0;
                    value.params.forEach(function(param){
                        params.push(matchRouter[i+1]);
                        i++;
                    });
                }

                return true
            }

        })[0];


        if (typeof selectedRoute.fn == 'string') {

            var method = selectedRoute.fn.split('@')[1];
            var controller = selectedRoute.fn.split('@')[0];

            var func = $controller._controllers[controller].fn;
            var args = $di.getServices(func);

            $controller._controllers[controller].fn.apply(null, args)[method].apply(null, params);

        } else if (typeof selectedRoute.fn == 'function') {

            selectedRoute.fn.apply(null, params);

        }
    }

    var bindChange = function(){
        var hashchange = $(window).bind("hashchange", function (e){
            urlChange();
        });

        if (window.location.hash.substring(1) == '') {
            window.location.hash = '#/';
        } else {
            hashchange.trigger('hashchange');
        }
    }


    return {

        add: function (path, fn) {
            _routes.push({path: path, regex: pathToRegExp(path), params: getParams(path), fn: fn});
        },

        _initialize: function () {

            bindChange();

        }
    }

})();

var $service = {

    _services: {},

    add: function (name, fn) {
        this._services[name] = {name: name, fn: fn};
    },

    setDI: function(){
        for (var prop in this._services){
            var args = $di.getServices(this._services[prop].fn);
            this._services[prop].fn = this._services[prop].fn.apply(this,args);
        };
    }

}


var $controller = {

    _controllers: {},

    add: function (name, fn) {
        this._controllers[name] = {name: name, fn: fn};
    }

}

var $view = {

    $config: {

        _viewPath: '/app/views/',
        _viewContent: 'view'

    },

    render: function (file) {
        $(this.$config._viewContent).load(this.$config._viewPath + file + '.html');
    }

}

var $di = (function(){

    var deps = [];

    return {
        getServices: function(func){
            var args = [];
            deps = func.toString().match(/^function\s*[^\(]*\(\s*([^\)]*)\)/m)[1].replace(/ /g, '').split(',');

            if (deps.length > 0) {
                for (var i = 0; i < deps.length; i++) {
                    var d = deps[i];
                    if (d !== ''){
                        args.push($service._services[d].fn);
                    }
                }
            }

            return args;
        }
    }

})();

var $app = {

    $data: {},

    _route: $route,
    _service: $service,
    _controller: $controller,
    _view: $view,

    $config: {

        _viewPath: '/app/views/',
        _viewContent: 'view',

        elBind: 'data-bind',
        elRepeat: 'data-repeat',
        elDataItem: 'data-item'

    },

    _fnstart: Function,

    $start: function(fn){
        this._fnstart = fn;
    },

    $route: function (path, fn) {
        this._route.add(path, fn);
    },

    $service: function (name, fn) {
        this._service.add(name, fn);
    },

    $controller: function (name, fn) {
        this._controller.add(name, fn);
    },

    $view: function (file) {
        this._view.render(file);
    },

    _run: function(){
        this._fnstart();
    },


    _initialize: function () {

        // view configurations
        this._view.$config._viewContent = this.$config._viewContent;
        this._view.$config._viewPath = this.$config._viewPath;

        //initial method in app start
        this._run();

        //services
        this._service.setDI();

        // routes
        this._route._initialize();


    }

};

$(function(){
    $app._initialize();
});




var utils = {

    executeFunctionByName: function (functionName, context /*, args */) {
        var args = [].slice.call(arguments).splice(2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(this, args);
    },

    fetchFromObject: function (obj, prop) {
        //property not found
        if (typeof obj === 'undefined') return false;

        //index of next property split
        var _index = prop.indexOf('.')

        //property split found; recursive call
        if (_index > -1) {
            //get object at property (before split), pass on remainder
            return this.fetchFromObject(obj[prop.substring(0, _index)], prop.substr(_index + 1));
        }

        //no split; get property
        return obj[prop];
    }

}